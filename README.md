# Aide et ressources de PyFAI pour Synchrotron SOLEIL

## Résumé

- réduction des données des détecteurs images 2D
- Open source

## Sources

- Code source: https://github.com/silx-kit/pyFAI
- Documentation officielle: https://www.silx.org/doc/pyFAI/latest/index.html

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](https://www.silx.org/doc/pyFAI/latest/operations/index.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/73/pyfai) |
| [Tutoriaux officiels](https://www.silx.org/doc/pyFAI/latest/index.html) |  |

## Installation

- Systèmes d'exploitation supportés:
- Installation: gérer dans la package déployé par GRADES

## Format de données

- en entrée: image *.edf
- en sortie: *.edf sinon image intégré sous for me de tableau de donnée I = f(Theta, Q) au *.txt ou *.dat
- sur la Ruche
